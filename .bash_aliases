#!/bin/bash

# enable color support of ls and also add handy aliases
# if [ -x /usr/bin/dircolors ]; then
    # test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls -G'
    alias dir='dir --color=auto'
    alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
# fi

alias gr='grep  --color=auto -iRnH'

function mkcd 
{
    if [ ! -n $1 ]; then
        echo "Enter a directory name"
    elif [ -d $1 ]; then
        echo "\`$1' already exists"
    else
        command mkdir $1 && cd $1
    fi
}
# Updates depnding on Linux/ mac
if [ "$(uname)" == "Darwin" ]; then
    alias update='brew update; brew upgrade; pip-review -a'
elif [ "$(uname)" == "Linux" ]; then
    alias update='sudo apt-get update; sudo apt-get -y upgrade'
fi


# some more ls aliases
alias ll='ls -FGlAhp'
alias la='ls -A'
alias l='ls -CF'
alias lr='ls -R | grep ":$" | sed -e '\''s/:$//'\'' -e '\''s/[^-][^\/]*\//--/g'\'' -e '\''s/^/   /'\'' -e '\''s/-/|/'\'' | less'

# Easier navigation
alias ..="cd .."
alias ...="cd ../.."
alias ....="cd ../../.."
alias .....="cd ../../../.."
alias ~="cd ~"
alias -- -="cd -"

# Current directory to clipbard
alias cpwd="pwd | tr -d '\n' | pbcopy"


# Use ipython
alias python="ipython"

# Use GUI Emacs
alias emacs="open -a /Applications/Emacs.app" 

# Use unzip 6
alias unzip6="/usr/local/Cellar/unzip/6.0/bin/unzip"

# remake build dir
alias rmbuild="cd ..; rm -rf build; mkcd build"

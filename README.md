# Matt's dotfiles

## Inside
- **bash** setup.
- **oh-my-zsh** setup
- **vimrc**
## Install

All files are kept in the ~/.dotfiles folder and symlinked into the home dir.

Run this:

'''sh
git@bitbucket.org:bluedreams/.dotfiles.git
cd ~/.dotfiles
./make_sym_links.sh'''
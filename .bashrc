# set 256 color profile where possible
if [[ $COLORTERM == gnome-* && TERM == xterm ]] && infocmp gnome-256color >/dev/null 2>&1; then
    export TERM=gome-256color
elif infocmp xterm-256 >/dev/null 2>&1; then
    export TERM=xterm-256color
fi

source ~/.bash_prompt
source $HOME/.dotfiles/.bash_aliases

# Enable vi keybindings
# set editing-mode vi

# don't put duplicate lines in the history. See bash(1) for more options
# ... or force ignoredups and ignorespace
HISTCONTROL=ignoredups:ignorespace

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize



# Alias definitions.
# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.



#export PATH=/usr/bin:/bin:/usr/local/bin:$PATH
### Added by the Heroku Toolbelt
export PATH="/usr/local/heroku/bin:$PATH"
export GTAGSLIBPATH="$HOME/.gtag"
export DYLD_FALLBACK_LIBRARY_PATH=/usr/local/cuda/lib:$DYLD_FALLBACK_LIBRARY_PATH

# Matlab stuff

export LD_LIBRARY_PATH=/usr/local/lib:$LD_LIBRARY_PATH
export CFLAGS="-I/usr/local/include"
export CXX_FLAGS="I/usr/local/include"
export LDFLAGS="-L/usr/local/lib"

PATH=$PATH:$HOME/.rvm/bin # Add RVM to PATH for scripting
# export PYTHONPATH=/usr/local/lib/python2.7/site-packages:$PYTHONPATH
export PATH=/usr/local/bin:/usr/local/sbin:/usr/local/share:$PATH
EDITOR="/usr/local/bin/vim"
export PATH=/Developer/NVIDIA/CUDA-6.5/bin:$PATH
export DYLD_LIBRARY_PATH=/Developer/NVIDIA/CUDA-6.5/lib:$DYLD_LIBRARY_PATH

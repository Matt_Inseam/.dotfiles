set nocompatible
filetype off
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" Vundle stuff
"
" let vundle manage Vundle
" required!
Plugin 'VundleVim/Vundle.vim'

"My Plugins here:
"
" original repos on github

"Plugin 'Conque-Shell'
"obsession.vim
"

"NERDtree
Plugin 'scrooloose/nerdtree'
map <C-n> :NERDTreeToggle<CR>

" Obsession.vim
Plugin 'tpope/vim-obsession'
" Surround.vim
Plugin 'tpope/vim-surround'

" CtrlP
Plugin 'kien/ctrlp.vim'
let g:ctrlp_match_window = 'bottom,order:ttb'
let g:ctrlp_switch_buffer = 0
let g:ctrlp_working_path_mode = 0
let g:ctrlp_custom_ignore = '\vbuild/|dist/|venv/|\.(o|swp|pyc|egg)$'

" Powerline
" Plugin 'Lokaltog/vim-powerline'

" vim-airline
Plugin 'bling/vim-airline'
let g:airline#extensions#tabline#enabled = 1


" Syntastic
Plugin 'scrooloose/syntastic'

"Plugin 'Shougo/neocomplete'
"
" YouCompleteMe
Plugin 'Valloric/YouCompleteMe'
let g:ycm_autoclose_preview_window_after_completion = 1

" vim-colorschemes
Plugin 'flazz/vim-colorschemes'


call vundle#end()
filetype plugin indent on


syntax on
set nocp

set nowritebackup
set noswapfile
set nobackup
set hidden

set backspace=indent,eol,start " more powerful backspace

""" SPACES AND TABS """
set tabstop=4
set softtabstop=4
autocmd FileType ruby setlocal shiftwidth=2 tabstop=2
set shiftwidth=4
set expandtab " Make tabs into spaces (set by tabstop)
set smarttab "Smarter tab levels
set foldmethod=indent
set foldlevel=99

set autoindent
set cindent
set cinoptions=:s,ps,ts,cs

""" UI CONFIG """
set number
set showmatch
set matchtime=5
set laststatus=2
set ruler
set showcmd
set cursorline
set wildmenu
set lazyredraw
set mousehide
set laststatus=2

""" SEARCHING """
set hlsearch
set ignorecase
set smartcase
set incsearch

""" COLORS """
set t_Co=256 " Explicitly tell Vim that the terminal supports 256 colors
" set background=dark
highlight Normal ctermfg=grey ctermbg=black
colorscheme zenburn
" let g:solarized_termcolors = 256
" let g:solarized_visibility = "high"
" let g:solarized_contrast = "high"

""" LEADER SHORTCUTS """
let mapleader="," " leader is comma
inoremap jk <esc> " jk is escape
nnoremap <leader>s :mksession<CR> " save session

" Key bindings
:nnoremap <C-l> :nohl<CR><C-l>

augroup configgroup
    au VimEnter * highlight clear SignColumn
    " au BufWritePre *.php,*.py,*.js,*.txt,*.hs,*.java,*.md :call <SID>CleanFile()
    au FileType ruby set tabstop=2
    au FileType ruby set shiftwidth=2
    au FileType ruby set softtabstop=2
    au FileType ruby set commentstring=#\ %s
    au FileType python set commentstring=#\ %s
    au BufEnter Makefile set noexpandtab
    au BufEnter *.sh set tabstop=2
    au BufEnter *.sh set shiftwidth=2
    au BufEnter *.sh set softtabstop=2
augroup END



"C++"
"Plugin 'octol/vim-cpp-enhanced-highlight' 
"Plugin 'Rip-Rip/clang_complete'
"let g:clang_complete_auto = 0
"let g:clang_complete_copen = 1

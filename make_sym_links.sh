#!/bin/bash
# This scrupt creates symlinkes from home directory to dotfiles

## Variables
dir=~/.dotfiles
olddir=~/.dotfiles_old
files="bashrc bash_prompt bash_aliases bash_profile vimrc gitignore vim tmux tmux.conf"

# create dotfiles_old in homedir
echo "Creating $olddir for backup of any existing dotfiles in ~"
mkdir -p $olddir
echo "...Done"

# change dotfiles directory
echo "Changing to the $dir directory"
cd $dir
echo "..done"

# move any existing dotfiles in homedir to dotfiles_old directory, then create symlinks
for file in $files; do
  echo "Moving any existing dotfiles from ~ to $olddir"
  mv ~/.$file ~/.dotfiles_old
  echo "Creating symlink to $file in home directory"
  ln -s $dir/.$file ~/.$file
done

# download vundle
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
